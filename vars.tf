variable "domain" {
  type    = string
  default = "uniquedevbox.com"
}
variable "domain_name" {
  type    = string
  default = "wpaws.uniquedevbox.com"
}
variable "page_name" {
  type    = string
  default = "wpaws"
}
variable "domain_mail" {
  type    = string
  default = "javier.rodriguez@uniquesoftwaredev.com"
}

variable "region" {
  description = "The region for the deployment"
  default     = "us-west-2"
}

variable "vpc_id" {
  description = "The VPC ID where WordPress will reside"
  default     = "vpc-43db7d26"
}

variable "ami_id" {
  description = "The AMI ID for AWS Linux 2 in us-west-2. In other regions, the ID is different"
  default     = "ami-a9d09ed1"

}

variable "instance_type" {
  description = "AWS Instance type to be used for the WordPress instance"
  default     = "t2.medium"
}

variable "volume_size" {
  description = "EBS volume size in GBs for the instance"
  default     = 8
}

variable "key_name" {
  description = "The key pair that will be used to log to the server using SSH"
  default     = "AWS-WORDPRESS"
}

variable "ssh_port" {
  description = "The SSH port for the server"
  default     = 22
}

variable "http_port" {
  description = "The HTTP port for the server"
  default     = 80
}

variable "https_port" {
  description = "The HTTPS port for the server"
  default     = 443
}

variable "mysql_port" {
  description = "The MySQL port for the database"
  default     = 3306
}

variable "nfs_port" {
  description = "The NFS port for the shared filesystem"
  default     = 2049
}

variable "allocated_storage" {
  description = "The size in GBs of the SQL database"
  default     = 10
}

variable "instance_class" {
  description = "The size/type of the SQL instance"
  default     = "db.t2.micro"
}

variable "db_admin" {
  description = "The dbadmin username"
  default     = "dbadmin"
}

variable "db_password" {
  description = "The dbadmin password"
  default     = "SuperSecretLel"
}

variable "db_name" {
  description = "The database name"
  default     = "dbwordpress"
}

variable "godaddy_key" {
  description = "The godaddy key"
  default     = "A4vTCN8AGV5_SUyKD1NSdEMmD3jDwkfs7c"
}

variable "godaddy_secret_key" {
  description = "The godaddy secret key"
  default     = "S4ASM6uwQ6mnk8u2MtL4iU"
}