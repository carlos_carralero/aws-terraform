provider "aws" {
  region = var.region
}
provider "acme" {
  server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
}
provider "google" {
  project = "acme-app"
  region  = var.region
}
resource "aws_security_group" "sgWordPress" {
  name   = "sgWordPress"
  vpc_id = var.vpc_id

  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = var.https_port
    to_port     = var.https_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.mysql_port
    to_port   = var.mysql_port
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = var.nfs_port
    to_port   = var.nfs_port
    protocol  = "tcp"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sgWordPress"
  }
}

resource "aws_efs_file_system" "efsWordPress" {
  creation_token = "EFS for WordPress"

  tags = {
    Name = "EFS for WordPress"
  }
}

data "aws_subnet_ids" "suballIDs" {
  vpc_id = var.vpc_id
}

resource "aws_efs_mount_target" "mtWordPress" {
  count          = length(data.aws_subnet_ids.suballIDs.ids)
  file_system_id = aws_efs_file_system.efsWordPress.id
  subnet_id       = sort(data.aws_subnet_ids.suballIDs.ids)[count.index]
  security_groups = [aws_security_group.sgWordPress.id]
}

resource "aws_instance" "wordpress" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.sgWordPress.id]
  key_name               = var.key_name
  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = var.volume_size
    delete_on_termination = "true"
  }
    tags = {
    Name = "var.domain_name"
  }
   user_data = <<EOF
        #!/bin/bash
          ##Variables
          domain='${var.domain}'
          domain_name='${var.domain_name}'
          mail='${var.domain_mail}'
          page_name='${var.page_name}'
          key='${var.godaddy_key}'
          secret='${var.godaddy_secret_key}'
          mysql_admin_password='${var.db_password}'
          mysql_user_password='${var.db_password}'
        echo $domain,$domain_name,$mail,$page_name,$key,$secret >> /variables.txt
        sudo yum update -y
        sudo amazon-linux-extras install epel -y
        echo "${aws_efs_file_system.efsWordPress.dns_name}:/ /var/www/html nfs defaults,vers=4.1 0 0" >> /etc/fstab
        ###  INSTALLING MYSQL & PHP  ###
        sudo yum install -y php php-dom php-gd php-mysql
        sudo yum install -y https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
        sudo yum install -y mysql-community-server 
        sudo systemctl start mysqld    
        sudo systemctl enable mysqld
        sudo grep 'A temporary password' /var/log/mysqld.log >> /home/ec2-user/password.txt
        password=$(awk 'NF>1{print $NF}' password.txt)
        sudo mysql -u root -p'$password'
        ALTER USER  'root'@'localhost' IDENTIFIED BY 'MySPassWD@69';
        CREATE DATABASE wordpress CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;
        CREATE USER 'wordpress'@'localhost' identified by 'MySPassWD@69';
        GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'localhost';
        FLUSH PRIVILEGES;
        EXIT;
                  ###  INSTALLING LET'S-ECNRYPT  ###            
        sudo yum install certbot python2-certbot-apache mod_ssl -y
        for z in {0..120}; do
            echo -n .
            host "${aws_efs_file_system.efsWordPress.dns_name}" && break
            sleep 1
        done
        cd /tmp
        wget https://wordpress.org/wordpress-4.6.20.tar.gz
        mount -a
        tar xzvf /tmp/wordpress-4.6.20.tar.gz --strip 1 -C /var/www/html
        rm /tmp/wordpress-4.6.20.tar.gz
        sudo chown -R apache:apache /var/www/html
        sudo chmod -R 777 /var/www/html
        sudo systemctl start httpd
        sudo systemctl enable httpd
        sudo echo "
          <VirtualHost *:80>
              RewriteEngine On
          </VirtualHost>
         " >> /etc/httpd/conf/httpd.conf
         sudo systemctl restart httpd.service
         sudo yum update -y
        sed -i 's/#ServerName www.example.com:80/ServerName www.myblog.com:80/' /etc/httpd/conf/httpd.conf
        sed -i 's/ServerAdmin root@localhost/ServerAdmin admin@myblog.com/' /etc/httpd/conf/httpd.conf
          ###  FIREWALL RULES  ###
        sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT
        sudo iptables -A INPUT -p tcp --dport 443 -j ACCEPT
        sudo iptables-save
          ### WE NEED CONFIGURED THE A REGISTRY WITH THE RIGHT IP ON THE DNS DOMAIN ###
        ip_var=$(sudo curl ifconfig.me)
		curl -X PUT "https://api.godaddy.com/v1/domains/$domain/records/A/$page_name" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: sso-key $key:$secret" -d "[ { \"data\": \"$ip_var\", \"name\": \"$page_name\", \"port\": 1, \"priority\": 0, \"protocol\": \"string\", \"service\": \"string\", \"ttl\": 600, \"type\": \"A\", \"weight\": 1 } ]"
        sudo certbot certonly --webroot --agree-tos -d $domain_name -w /var/www/html -m $mail -n
        sudo chmod -R 777 /etc/letsencrypt
        sudo chmod 777 /etc/httpd/conf/httpd.conf
        sudo echo "
          <VirtualHost *:443>

                  DocumentRoot /var/www/html/
                  ServerName $domain_name

                  SSLCertificateFile /etc/letsencrypt/live/$domain_name/fullchain.pem
                  SSLCertificateKeyFile /etc/letsencrypt/live/$domain_name/privkey.pem
                  SSLCertificateChainFile /etc/letsencrypt/live/$domain_name/chain.pem

          </VirtualHost> 
          " >> /etc/httpd/conf/httpd.conf
        sudo systemctl restart httpd.service
          ### THE END ###
		  EOF
}

resource "aws_eip" "lb" {
  instance = aws_instance.wordpress.id
  vpc      = true
 }